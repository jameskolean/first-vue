I have worked in React for several years and was recently called upon to help on a Vue project. I wrote my first VueJs app, a note app using Pinia for state management and Tailwind. I started using `pnpm` too. I am trying to keep an open mind and am pretty happy with Vue, but I can't recommend creating a new project in Vue in 2023. Adoption rates for both Vue and React are on the decline. With the Vue community just a fraction of the React community size, it would not be responsible to lock an organization into a tech stack that may not be around in 5 years. Vue doesn't do anything radically different, so I don't see any reason to switch there. Innovation in the React space is going to outpace anything in Vue. If you are looking for the next big thing, you should look beyond Angular, React, and Vue. What that thing is, is unclear to me now. There is potential in Remix, Astro, and 11ty. 

Let's get started building my Vue app.

## Create App

```shell
npm init vue@latest first-vue
Vue.js - The Progressive JavaScript Framework

✔ Add TypeScript? … No / _Yes_
✔ Add JSX Support? … _No_ / Yes
✔ Add Vue Router for Single Page Application development? … No / _Yes_
✔ Add Pinia for state management? … No / _Yes_
✔ Add Vitest for Unit Testing? … No / _Yes_
✔ Add an End-to-End Testing Solution? › No
✔ Add ESLint for code quality? … No / _Yes_
✔ Add Prettier for code formatting? … No / _Yes_

Scaffolding project in .../first-vue...

Done. Now run:

  cd first-vue
  npm install
  npm run lint
  npm run dev
```

That gets us close, but I want to use `pnpm` and `Tailwind.` I want a modal component, so let's pull that in too.

```shell
cd first-vue
brew install pnpm
pnpm  install
pnpm install -D tailwindcss concurrently
pnpm install vue-final-modal@3
```

## Configure Tailwind and Modal

```javascript
/* tailwind.config.js */
/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.{html,js,vue}"],
    theme: {
        extend: {},
    },
    plugins: [],
};
```

```css
/* tailwind.css */
@tailwind base;
@tailwind components;
@tailwind utilities;
```

Edit package.json

```json
...
  "scripts": {
    "tailwind": "npx tailwindcss -i ./tailwind.css -o ./src/assets/tailwind.css",
    "dev": "concurrently --kill-others \"npm run tailwind --watch\" \"vite\"",
    "build": "run-p tailwind type-check build-only",
...
```

```typescript
// src/main.ts
import { createPinia } from "pinia";
import { createApp } from "vue";
import { vfmPlugin } from "vue-final-modal";
import App from "./App.vue";
import router from "./router";

import "./assets/main.css";
import "./assets/tailwind.css";

const app = createApp(App);

app.use(createPinia());
app.use(router);

app.use(
  vfmPlugin({
    key: "$vfm",
    componentName: "VueFinalModal",
    dynamicContainerName: "ModalsContainer",
  })
);
app.mount("#app");
```
