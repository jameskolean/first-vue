import NoteCard from "@/components/NoteCard.vue";
import type { State } from "@/stores/note";
import { useNoteStore } from "@/stores/note";
import { mount } from "@vue/test-utils";
import { createPinia, setActivePinia } from "pinia";
import { beforeEach, describe, expect, it } from "vitest";

describe("NoteCard", () => {
  let store: State | null = null;
  beforeEach(() => {
    // create a fresh Pinia instance and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia());

    // create an instance of the data store
    store = useNoteStore();
  });
  it("renders properly", () => {
    const wrapper = mount(NoteCard, {
      props: { note: { title: "First Note", body: "This is my first note." } },
    });
    expect(wrapper.find("h5").text()).toContain("First Note");
    expect(wrapper.find("p").text()).toContain("This is my first note.");
  });
  it("initializes store with empty array of note", () => {
    expect(store).not.toBeNull();
    expect(store?.notes).not.toBeNull();
    expect(store?.notes.length).toBe(0);
  });
});
