// src/stores/note.ts
import { defineStore } from "pinia";

export interface Note {
  title: string;
  body: string;
}
export interface State {
  notes: Note[];
}

export const useNoteStore = defineStore("note", {
  state: (): State => ({
    notes: [],
  }),
  actions: {
    addNote(title: string, body: string) {
      const existingNoteIndex = this.notes.findIndex(
        (note) => note.title === title
      );
      if (existingNoteIndex >= 0) {
        const newNotes = [
          ...this.notes.slice(0, existingNoteIndex),
          { title, body },
          ...this.notes.slice(existingNoteIndex + 1),
        ];
        this.notes = newNotes;
      } else {
        this.notes.push({ title, body });
      }
    },
    removeNote(title: string) {
      const newNotes = this.notes.filter((note) => note.title !== title);
      this.notes = newNotes;
    },
  },
});
